numRows = numCols = 10
turns = 100

array1 = Array.new(numRows) {Array.new(numRows)}
array2 = Array.new(numRows) {Array.new(numRows)}
dx = [-1,-1,-1,0,1,1,1,0]
dy = [-1,0,1,1,1,0,-1,-1]
def init(arr, numRows)
	for i in 0...numRows
		for j in 0...numRows
			arr[i][j] = Random.new.rand(0..1)
		end
	end		
end




def check(i,j,dx,dy, numRows, arr)
	count = 0
    for dir in 0...8
    	x0 = i + dx[dir]
        y0 = j + dy[dir]
        if (x0 >= 0 and x0 < numRows and y0 >= 0 and y0 < numRows)
        	if (arr[x0][y0] == 1)
        		count += 1
            end		
        end
    end
    if arr[i][j] == 1
    	if count == 2 or count == 3
    		return true
    	else 
    		return false
    	end
    else
    	if count == 3
    		return true
    	else
    		return false
    	end
    end		    
end

def render(arr)
	print "<table border='1' style='padding: 2px'>"
	numAlive = 0
	arr.each do |sub|
		print "<tr>"
		sub.each do |int|
			if int == 1
				print "<td style='padding: 4px' bgcolor='green'>"
				print "<span style='color: green'>"
				numAlive += 1
			else
				print "<td style='padding: 4px' bgcolor='red'>"
				print "<span style='color: red'>"
			end
				print "#{int}"
				print "</span>"
				print "</td>"
		end
		print "</tr>"
		print "\n"
	end
	print "</table>"
	print ("<span style='color: blue'>#{numAlive} cells are still alive</span>")
end

print "<h1>#{turns} turns of Conway:</h1>\n"
print "(starting from a random seed of 1s and 0s)\n\n"
init(array1, numRows)
for turn in 0...turns
	print "<h2>\nTurn ##{turn}:</h2>"
	for i in 0...numRows
		for j in 0...numRows
			if check(i,j,dx,dy,numRows,array1)
				array2[i][j] = 1
			else
			    array2[i][j] = 0
			end
		end
	end
	print "\n"
	render(array2)
	for i in 0...numRows
		for j in 0...numRows
			array1[i][j] = array2[i][j]
		end
	end		
end

